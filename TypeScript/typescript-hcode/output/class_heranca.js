"use strict";
class Cadastro {
    constructor(nome, nascimento) {
        this.nome = nome;
        this.nascimento = nascimento;
    }
}
class Client extends Cadastro {
    constructor(nome, nascimento, email, empresa) {
        super(nome, nascimento);
        this.email = email;
        this.empresa = empresa;
    }
}
const clientJoao = new Client("João", new Date("2000-01-01"), "joao@hcode.com.br", "HCode");
//# sourceMappingURL=class_heranca.js.map