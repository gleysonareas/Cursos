"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BancoDeDados = void 0;
class BancoDeDados {
    constructor(ip, user, senha, tipoBanco) {
        this.ip = ip;
        this.user = user;
        this.senha = senha;
        this.tipoBanco = tipoBanco;
    }
    static factory(parametros) {
        if (![BancoDeDados.Tipo_MYSQL, BancoDeDados.Tipo_SQLSERVER].includes(parametros.tipoBanco)) {
            throw new Error("Tipo de banco incorreto");
        }
        return new BancoDeDados(parametros.ip, parametros.user, parametros.senha, parametros.tipoBanco);
    }
}
exports.BancoDeDados = BancoDeDados;
BancoDeDados.Local = "127.0.0.1";
BancoDeDados.Tipo_MYSQL = "mysql";
BancoDeDados.Tipo_SQLSERVER = "sqlserver";
//# sourceMappingURL=banco_de_dados.js.map