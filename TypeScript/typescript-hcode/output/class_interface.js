"use strict";
class NotificacaoV2 {
}
class EmailV2 extends Notificacao {
    constructor(usuario) {
        super();
        this.nome = usuario.nome;
        this.email = usuario.email;
    }
    enviar() {
        console.log(`Enviando e-mail para o usuario ${this.email}...`);
        return true;
    }
}
class SMSV2 extends Notificacao {
    constructor(usuario) {
        super();
        this.telefone = usuario.email;
    }
    enviar() {
        console.log(`Enviando SMS para o usuario ${this.telefone}...`);
        return true;
    }
}
new EmailV2({
    nome: "string",
    email: "string",
    telephone: "string",
}).enviar();
new SMSV2({
    nome: "string",
    email: "string",
    telephone: "string",
}).enviar();
//# sourceMappingURL=class_interface.js.map