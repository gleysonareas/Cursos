"use strict";
var Authentication;
(function (Authentication) {
    class LoginRegister {
        login(user) {
            return user;
        }
        register(newUser) {
            return newUser;
        }
    }
    Authentication.LoginRegister = LoginRegister;
    class Recover {
        recoverPassword() {
            return "Enviando e-mail para recuperação de senha";
        }
        recoverUser() {
            return "Enviando e-mail para recuperação de usuario";
        }
    }
    Authentication.Recover = Recover;
})(Authentication || (Authentication = {}));
//# sourceMappingURL=index.js.map