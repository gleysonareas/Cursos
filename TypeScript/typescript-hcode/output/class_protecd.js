"use strict";
class Domicilio {
    constructor(cor, endereco) {
        this.cor = cor;
        this.endereco = endereco;
    }
    tocarInterfone() {
        return "Interfone tocado";
    }
}
class Casa extends Domicilio {
    atenderInterfone(mensagem) {
        return mensagem;
    }
    entrarNaCasa() {
        return this.atenderInterfone("Oi, quem é?");
    }
}
const casaDoHomer = new Casa("Rosa", { cidade: "Spring Field" });
console.log(casaDoHomer.tocarInterfone());
console.log(casaDoHomer.entrarNaCasa());
//# sourceMappingURL=class_protecd.js.map