"use strict";
class Pessoa {
    constructor(nome, idade, altura) {
        this.nome = nome;
        this.idade = idade;
        this.altura = altura;
    }
    toString() {
        return `A Pessoa ${this.nome} tem ${this.idade} anos e mede ${this.altura} metros`;
    }
}
const ronaldo = new Pessoa("Ronaldo Braz", 30, 1.85);
const marcus = new Pessoa("Marcus Ribeiro", 19, 2);
ronaldo.nome = "Ronaldo";
console.log(`${ronaldo}`);
console.log(`${marcus}`);
//# sourceMappingURL=first_class.js.map