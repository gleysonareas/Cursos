"use strict";
class Permissao {
    constructor(_nome, _nivel) {
        this._nome = _nome;
        this._nivel = _nivel;
    }
    get nome() {
        return this._nome.toUpperCase();
    }
    set nome(novoNome) {
        if (novoNome.length < 5) {
            throw new Error("O nome da permissão deve ter no minimo 5 letras");
        }
        this._nome = novoNome;
    }
}
const permissao = new Permissao("Admnistrador", 1);
console.log(permissao.nome);
try {
    permissao.nome = "adm";
}
catch (e) {
    console.log(e.message);
}
console.log(permissao.nome);
//# sourceMappingURL=class_getters_setters.js.map