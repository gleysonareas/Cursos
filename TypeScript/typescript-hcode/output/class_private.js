"use strict";
class Banco {
    constructor() {
        this.cofreQtd = 10000;
    }
    debitarCofre(quantidade) {
        if (quantidade <= this.cofreQtd) {
            this.cofreQtd -= quantidade;
            return `O cofre agora possui ${this.cofreQtd} valor`;
        }
        else {
            return "O cofre não possui a quantidade requisitada";
        }
    }
    sacarDoCaixa(quantidade) {
        return this.debitarCofre(quantidade);
    }
    sacarDoCaixaEletronico(quantidade) {
        return this.debitarCofre(quantidade);
    }
}
class Banco24Horas extends Banco {
    sacar(qtd) {
        return this.sacarDoCaixa(qtd);
    }
}
const nubank = new Banco();
const banco24Horas = new Banco24Horas();
banco24Horas.sacar(2500);
//# sourceMappingURL=class_private.js.map