"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const notification_1 = __importDefault(require("./notification"));
class SMS extends notification_1.default {
    enviar(usuario) {
        console.log(`Enviando SMS para o usuario ${usuario.email}...`);
        return true;
    }
}
exports.default = SMS;
//# sourceMappingURL=sms.js.map