"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Android = exports.SMS = exports.Email = void 0;
const notification_1 = __importDefault(require("./notification"));
class Email extends notification_1.default {
    enviar(usuario) {
        console.log(`Enviando e-mail para o usuario ${usuario.email}...`);
        return true;
    }
}
exports.Email = Email;
class SMS extends notification_1.default {
    enviar(usuario) {
        console.log(`Enviando SMS para o usuario ${usuario.email}...`);
        return true;
    }
}
exports.SMS = SMS;
class Android extends notification_1.default {
    enviar(usuario) {
        console.log(`Enviando mensagem para o usuario no android ${usuario.email}...`);
        return true;
    }
}
exports.Android = Android;
//# sourceMappingURL=midles_notification.js.map