"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const notification_1 = __importDefault(require("./notification"));
class Android extends notification_1.default {
    enviar(usuario) {
        console.log(`Enviando mensagem para o usuario no android ${usuario.email}...`);
        return true;
    }
}
exports.default = Android;
//# sourceMappingURL=android.js.map