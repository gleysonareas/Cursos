import "reflect-metadata";

function decoratorsParameters(target: any, key: string, propertyKey: Number) {
  return Reflect.getMetadata("design:paramtypes", target, key)
    .map((item: any) => console.log(item));
}

class TratarParametro {
  metodo1(@decoratorsParameters mensagem:string) { }
  metodo2(@decoratorsParameters numero: number) { }
}

new TratarParametro();
