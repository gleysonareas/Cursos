export interface IBancoDeDados {
  ip: string;
  user: string;
  senha: string;
  tipoBanco: string;
}