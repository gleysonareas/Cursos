interface IModel {
  id: number;
  createdAt: number;
  updatedAt: number;
}

interface IPessoa extends IModel {
  nome: string;
  idade?: number;
}

interface IUser extends IPessoa {
  email: string;
  senha: string;
}

const joao: IUser = {
  email: 'joao@gmail.com',
  id: 1,
  nome: 'joao',
  senha: 'test',
  // idade: 30, 
  createdAt: new Date().getTime(),
  updatedAt: new Date().getTime()
}