import applyMixins from "./applymixins"
class ProdutoFinal {
  vender(quantidade: number) {
    return `Foram vendidos ${quantidade} itens de produto`;
  }
  comprar(quantidade: number) {
    return `Foram comprados ${quantidade} itens deste produto`;

  }
}

class Movel {
  sentar() {
    return `Voce sentou no móvel`;
  }
  empurrar(metros: number) {
    return `O movel foiempurrado ${metros}`;
  }
}

class Sofa {

  constructor(public nome: string) {
    // super();
  }
}
interface Sofa extends ProdutoFinal, Movel { }

applyMixins(Sofa, [ProdutoFinal, Movel]);

const produto = new Sofa("Meu Sofa");

produto.vender(25);
console.log(produto.empurrar(50));