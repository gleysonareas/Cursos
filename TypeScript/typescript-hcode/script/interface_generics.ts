interface IProcess<T> {
  valor: T
  realizarProcessamento(arg: T): T;
}

const texto: IProcess<string> = {
  valor: "",
  realizarProcessamento(arg: string): string {
    return arg.toUpperCase();
  }
}

console.log(texto.valor);
console.log(texto.realizarProcessamento("Treinamento HCODE"));

const numero: IProcess<number> = {
  valor: 10,
  realizarProcessamento(v: number): number {
    return v * v;
  }
}

console.log(numero.realizarProcessamento(10));