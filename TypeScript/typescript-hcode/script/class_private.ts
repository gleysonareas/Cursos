class Banco {
  private cofreQtd: number = 10000;

  private debitarCofre(quantidade: number) {
    if (quantidade <= this.cofreQtd) {
      this.cofreQtd -= quantidade;
      return `O cofre agora possui ${this.cofreQtd} valor`;
    } else {
      return "O cofre não possui a quantidade requisitada";
    }
  }

  protected sacarDoCaixa(quantidade: number) {
    return this.debitarCofre(quantidade);
  }

  public sacarDoCaixaEletronico(quantidade: number) {
    return this.debitarCofre(quantidade);
  }
}

class Banco24Horas extends Banco {
  sacar(qtd: number) {
    return this.sacarDoCaixa(qtd)
  }
}

const nubank = new Banco();
const banco24Horas = new Banco24Horas();

banco24Horas.sacar(2500);