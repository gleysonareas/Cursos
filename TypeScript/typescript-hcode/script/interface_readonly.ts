interface ICurso {
  readonly titulo: string; // Dessa forma definimos que uma propriedade não pode ser alterada apenas lida.
  descricao?: string;
  preco: number;
  cargaHoraria: number;
  classicacao: number;
}

const curso: ICurso = {
  titulo: "",
  preco: 0,
  cargaHoraria: 0,
  classicacao: 0
}

// curso.titulo = "PHP" // Dessa forma não podemos sobreescrever na atribuição ja realizada na definição do objeto.
console.log(curso);