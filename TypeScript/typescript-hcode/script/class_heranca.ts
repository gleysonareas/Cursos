class Cadastro {
  nome: string;
  nascimento: Date;

  constructor(nome: string, nascimento: Date) {
    this.nome = nome;
    this.nascimento = nascimento;
  }
}

class Client extends Cadastro {
  email: string;
  empresa: string;
  constructor(nome: string, nascimento: Date, email: string, empresa: string) {
    // Com o metodo super eu faço referencia ao constructor da class pai tendo inclusive 
    // caso necessario acesso ao recurso de passar valores para as propriedades definidas no mesmo.
    super(nome, nascimento);
    this.email = email;
    this.empresa = empresa;
  }
}

const clientJoao = new Client("João", new Date("2000-01-01"), "joao@hcode.com.br", "HCode");