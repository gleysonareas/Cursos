interface INotificacaoV2 {
  enviar(usuario: IMeuUsuarioV2): boolean;
}

interface IEmailV2 {
  nome: string;
  email: string;
}

interface ITelefoneV2 {
  telefone: string;
}

interface IMeuUsuarioV2 {
  nome: string;
  email: string;
  telephone: string;
  idAndroid?: string;
}

abstract class NotificacaoV2 implements INotificacaoV2 {
  abstract enviar(): boolean;
}

class EmailV2 extends Notificacao implements IEmailV2 {

  nome: string;
  email: string;

  constructor(usuario: IMeuUsuarioV2) {
    super();
    this.nome = usuario.nome;
    this.email = usuario.email;
  }

  enviar(): boolean {
    console.log(`Enviando e-mail para o usuario ${this.email}...`);
    return true;
  }
}

class SMSV2 extends Notificacao implements ITelefoneV2 {

  telefone: string;

  constructor(usuario: IMeuUsuarioV2) {
    super();
    this.telefone = usuario.email;
  }

  enviar(): boolean {
    console.log(`Enviando SMS para o usuario ${this.telefone}...`);
    return true;
  }
}

new EmailV2({
  nome: "string",
  email: "string",
  telephone: "string",
}).enviar()

new SMSV2({
  nome: "string",
  email: "string",
  telephone: "string",
}).enviar()