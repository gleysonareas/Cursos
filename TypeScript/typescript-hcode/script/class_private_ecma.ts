class Documento {
  private valor: string = "1236544789-01"; // TS 
  #numero: number = 35; // ECMAScript
  mostrarDocumento() {
    return this.#numero;
  }
}

class CNPJ extends Documento {
  // private valor: string = "1234657989879-01"
  #numero: number = 50;

  mostrarCNPJ() {
    return this.#numero;
  }
}

const rg = new Documento();
console.log(rg.mostrarDocumento());
const cnpj = new CNPJ();
console.log(cnpj.mostrarCNPJ());

// Ambas as chamadas são de propriedades privadas porem o encapsulamento do do TS na conversão independente
// de ser protegido ou privado ele será convertido para propriedades ou metodos publico no JS.
// console.log("RG" + rg.valor);
// console.log("RG" + rg.#numero);