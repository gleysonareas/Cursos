
// dessa maneira criamos um decorator
function debug(classe: unknown) {
  console.log("classe criada", classe);
}

function log(ctor: any) {
  return class extends ctor {
    created_at: Date = new Date("2021-02-15");
  }
}

// aqui realizamos a chamada do mesmo
@debug
class PrimeiraClass {

  constructor() {
    // super();

  }
}

@log
class SegundaClass {

  constructor() {
    // super();

  }
}