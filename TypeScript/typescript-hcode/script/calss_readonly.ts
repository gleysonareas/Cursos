class Usuario {
  name: string = "";
  readonly id: string = "";
  private senha: string = "";
  readonly dataCadastro: Date = new Date("2021-01-01");
}

const usuario = new Usuario();
// usuario.id = "123";
console.log(usuario.id);