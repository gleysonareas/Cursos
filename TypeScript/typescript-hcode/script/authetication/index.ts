namespace Authentication {
  interface IUser {
    email: string;
    password: string;
  }

  interface IRegister {
    name: string;
    email: string;
    password: string;
    nascimentDate: Date;
  }

  export class LoginRegister {
    login(user: IUser) {
      return user;
    }
    register(newUser: IRegister) {
      return newUser;
    }
  }

  export class Recover {
    recoverPassword() {
      return "Enviando e-mail para recuperação de senha"
    }

    recoverUser() {
      return "Enviando e-mail para recuperação de usuario"
    }
  }
}