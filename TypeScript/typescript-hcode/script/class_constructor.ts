class Pedido {
  // Ao encapsular as propriedades no CTOR eu não preciso necessariamente declarar a propriedade e fazer
  // a atribuição dela pois o TS assim como o JS irá fazer isso em segundo plano de forma implicita.

  // private produto: string;
  // protected valorTotal: number;
  // public previsaoEntrega: Date;

  constructor(
    private produto: string,
    protected valorTotal: number,
    public previsaoEntrega: Date
  ) {
    // this.produto = produto;
    // this.valorTotal = valorTotal;
    // this.previsaoEntrega = previsaoEntrega;
  }
}

const meuPedido = new Pedido("TV 64 polegadas", 2000, new Date("2021-05-01"));
console.log(meuPedido);