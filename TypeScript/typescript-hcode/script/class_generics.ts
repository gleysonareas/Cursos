interface CadastroPadrao{
  readonly id: number;
  readonly create_at: Date;
  readonly updated_at: Date;
}

interface CadastroUsuario extends CadastroPadrao {
  nome: string;
  email: string;
  senha: string;
}

interface CadastroCategoria extends CadastroPadrao {
  nome: string;
}

class CadastroBasico<BasicInterface> {
  create(dados: BasicInterface) {
    console.log("Criando novo registro");
    return dados;
  }
  read(id: number): BasicInterface {
    console.log("Dados de registro de ID " + id);
    return {} as BasicInterface;
  }

  edit(id: number, dados: BasicInterface) {
    console.log("Editando dados do ID " + id);
    return dados;
  }

  delete(id: number) {
    console.log("Excluindo registro do Id " + id);
    return true;
  }
}

class UserModel extends CadastroBasico<CadastroUsuario>{}
const newUser = new UserModel();
newUser.create({
  nome: "",
  email: "",
  senha: "",
  id: 0,
  create_at: new Date("2015-06-01"),
  updated_at: new Date("2015-06-10")
})

const newCategory = new CadastroBasico<CadastroCategoria>();
newCategory.create({
  nome: "",
  id: 0,
  create_at: new Date("2015-06-01"),
  updated_at: new Date("2015-06-10")
})