interface ISoma {
  (num1: number, num2: number): number;
}

let soma: ISoma;

// Como a interface ja defini o tipo de parametro e retorno da função, não é necessário definir isso novamente.
soma = (numero1, numero2) => {
  return numero1 + numero2;
}

console.log(soma);

interface ICaculos {
  somar(a: number, b: number): number;
  subtrair(a: number, b: number): number;
  multiplicar(a: number, b: number): number;
  dividir(a: number, b: number): number;
}

let calculadora: ICaculos;

function adcao(a: number, b: number): number {
  return a + b;
}

// Essa função não precisa ser atribuida pois tem o mesmo nome do metodo da assinatura, portando o typescript ja fara isso de forma automatica.
const subtrair = (n1: number, n2: number): number => n1 - n2

calculadora = {
  multiplicar: (a: number, b: number) => {
    return a * b;
  },
  // Tanto faz se na chamada isso vai ser uma Arrow ou não.
  dividir(a: number, b: number) {
    return a / b;
  },
  somar: adcao, // ainda vale lembrar que eu posso atribuir uma função como resultado de outra função
  subtrair, // vale lembrar que se eu tenho uma função com o mesmo nome da assinatura do metodo da interface eu não preciso chamar esse método novamente, pois implicitamente o typescript intend que isso ja foi implementado e segui a compilação normalmente
}