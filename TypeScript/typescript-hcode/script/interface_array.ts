interface ICategoria {
  nome: string;
  id: number;
  categoria?: ICategoria;
};

interface IMenu {
  categoria: ICategoria[];
};

interface ITodo {
  [indice: number]: string | ICategoria;
}

const frontEnd: ICategoria = {
  nome: 'front-end',
  id: 1,
};

const backEnd: ICategoria = {
  nome: 'back-end',
  id: 2,
};

let menu: IMenu = {
  categoria: [frontEnd, backEnd],
};

let minhasTarefas: ITodo;
minhasTarefas = [
  'Estudar typescript',
  'Estudar javascript',
  'Estudar PHP 8',
  frontEnd,
  {
    nome: '',
    id: 3,
    categoriaPai: frontEnd
  } as ICategoria];

console.log(minhasTarefas[4]);
