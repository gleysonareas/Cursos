class Domicilio {

  public cor: string;
  public endereco: object;

  constructor(cor: string, endereco: object) {
    this.cor = cor;
    this.endereco = endereco;
  }

  public tocarInterfone(): string {
    return "Interfone tocado";
  }
}

class Casa extends Domicilio {

  // Quando o metodo constructor não é definido na classe filha 
  // por padrão o constructor da classe pai será chamado automatico.

  protected atenderInterfone(mensagem: string): string {
    return mensagem;
  }
  public entrarNaCasa() {
    return this.atenderInterfone("Oi, quem é?");
  }
}

const casaDoHomer = new Casa("Rosa", { cidade: "Spring Field" });
console.log(casaDoHomer.tocarInterfone());
console.log(casaDoHomer.entrarNaCasa());
