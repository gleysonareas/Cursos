// Dessa forma é feito a referencia de um namespace no TS
// Daí é necessario compilar ele a parte de forma:
// tsc script/authentication/authentication.ts --outFile output/authentication.js
///<reference path="./authetication/index.ts"/>

const newRegister = new Authentication.LoginRegister();

newRegister.register({
  name: "",
  email: "",
  password: "",
  nascimentDate: new Date("1995-02-20")
})

console.log(newRegister);