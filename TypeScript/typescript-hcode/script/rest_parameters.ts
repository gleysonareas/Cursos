// function somarRenda(mes1: number, mes2: number, mes3: number): number {
//   return mes1 + mes2 + mes3;
// }
function somarRenda(...meses: number[]): number {

  //Com o metodo reduce é possivel somar todos os parametros passados dentro do metodo.
  return meses.reduce((rendaTotal, rendaAtual) => rendaTotal + rendaAtual, 0);
}



console.log(somarRenda(250, 780, 695));