class Veiculo {
  // Com relação ao modificador public se por padrão se voce não colocar nada ele será public.
  public cor: string;

  constructor(cor: string) {
    this.cor = cor;
  }

  public tentarAbrirPorta(): boolean {
    return false;
  }
}

const carro = new Veiculo("Azul");
carro.cor = "Preto";
console.log(carro.cor);
console.log(carro.tentarAbrirPorta());


