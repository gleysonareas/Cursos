import { IBancoDeDados } from "../interfaces/banco_de_dados.interface";

export class BancoDeDados {

  static Local = "127.0.0.1";
  static Tipo_MYSQL = "mysql";
  static Tipo_SQLSERVER = "sqlserver";

  constructor(
    private ip: string,
    private user: string,
    private senha: string,
    private tipoBanco: string
  ) { }

  static factory(parametros: IBancoDeDados) {

    if (![BancoDeDados.Tipo_MYSQL, BancoDeDados.Tipo_SQLSERVER].includes(parametros.tipoBanco)) {
      throw new Error("Tipo de banco incorreto");
    }
    return new BancoDeDados(
      parametros.ip,
      parametros.user,
      parametros.senha,
      parametros.tipoBanco
    );
  }
}