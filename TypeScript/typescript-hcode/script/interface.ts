// Essa é a forma padrão de criar uma interface
interface IEndereco {
  logradouro: string;
  numero: number;
  bairro: string;
  cidade: string
}

// Exemplo de interface local
// let endereco: {
//   logradouro: string,
//   numero: number,
//   bairro: string,
//   cidade: string
// };

// Essa é a forma padrão de definir tipos criados
let endereco: IEndereco;

endereco = {
  logradouro: "",
  numero: 0,
  cidade: "",
  bairro: "",
}

