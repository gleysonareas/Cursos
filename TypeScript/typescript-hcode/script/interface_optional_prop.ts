interface IProduct {
  nome: string;
  preco: number;
  descricao?: string
  dataValidade: Date;
}

const produtoDados: IProduct = {
  nome: "",
  preco: 0,
  // descricao: "", Essa propriedade é opcional
  dataValidade: new Date(2022, 11, 1), // O tipo date para ser definido, quando no caso do mês vai de 0 a 11 e não de 1 a 12.
};