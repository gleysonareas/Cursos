import IMeuUsuario from "../interfaces/meu_usuario.interface";

export default abstract class Notificacao {
  abstract enviar(usuario: IMeuUsuario): boolean;
}