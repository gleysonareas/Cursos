import IMeuUsuario from "../interfaces/meu_usuario.interface";
import Notificacao from "./notification";

export class Email extends Notificacao {
  enviar(usuario: IMeuUsuario): boolean {
    console.log(`Enviando e-mail para o usuario ${usuario.email}...`);
    return true;
  }
}

export class SMS extends Notificacao {
  enviar(usuario: IMeuUsuario): boolean {
    console.log(`Enviando SMS para o usuario ${usuario.email}...`);
    return true;
  }
}

export class Android extends Notificacao {
  enviar(usuario: IMeuUsuario): boolean {
    console.log(`Enviando mensagem para o usuario no android ${usuario.email}...`);
    return true;
  }
}