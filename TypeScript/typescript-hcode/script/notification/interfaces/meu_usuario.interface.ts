export default interface IMeuUsuario {
  nome: string;
  email: string;
  telephone: string;
  idAndroid?: string;
}