interface IMeuUsuario {
  nome: string;
  email: string;
  telephone: string;
  idAndroid?: string;
}

abstract class Notificacao {
  abstract enviar(usuario: IMeuUsuario): boolean;
}

class Email extends Notificacao {
  enviar(usuario: IMeuUsuario): boolean {
    console.log(`Enviando e-mail para o usuario ${usuario.email}...`);
    return true;
  }
}

class SMS extends Notificacao {
  enviar(usuario: IMeuUsuario): boolean {
    console.log(`Enviando SMS para o usuario ${usuario.email}...`);
    return true;
  }
}
class Android extends Notificacao {
  enviar(usuario: IMeuUsuario): boolean {
    console.log(`Enviando mensagem para o usuario no android ${usuario.email}...`);
    return true;
  }
}

new Email().enviar({
  nome: "string",
  email: "string",
  telephone: "string",
})

new SMS().enviar({
  nome: "string",
  email: "string",
  telephone: "string",
})

new Android().enviar({
  nome: "string",
  email: "string",
  telephone: "string",
  idAndroid: "string",
})