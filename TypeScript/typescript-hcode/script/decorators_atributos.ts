function decoratorAttr(target: unknown, nomePropriedade: string) {
  const newName = `_${nomePropriedade}`;
  Object.defineProperty(target, nomePropriedade, {
    get() {
      return this[newName].toUpperCase();
    },
    set(newValue) {
      this[newName] = newValue.split('').reverse().join('');
    }
  })
}

class Animal {
  @decoratorAttr
  nome: string;

  constructor(nome: string) {
    // super();
    this.nome = nome;
  }
}

const cachorro = new Animal("pluto");
cachorro.nome = "Snoopy";
console.log(cachorro.nome);
