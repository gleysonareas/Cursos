class Pessoa {

  nome: string;
  idade: number;
  altura: number;

  constructor(
    nome: string,
    idade: number,
    altura: number
  ) {
    this.nome = nome;
    this.idade = idade;
    this.altura = altura;
  }

  toString(): string {
    return `A Pessoa ${this.nome} tem ${this.idade} anos e mede ${this.altura} metros`;
  }
}

const ronaldo = new Pessoa("Ronaldo Braz", 30, 1.85);
const marcus = new Pessoa("Marcus Ribeiro", 19, 2);
ronaldo.nome = "Ronaldo";
console.log(`${ronaldo}`);
console.log(`${marcus}`);