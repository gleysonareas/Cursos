function decoratorGetSet(value: boolean) {
  return function (target: unknown, propertyKey: string, descriptor: PropertyDescriptor) {
    descriptor.enumerable = value;
  }
}
class Login {
  constructor(
    private _user: string,
    private _pass: string
  ) { }

  @decoratorGetSet(true)
  get user() {
    return this._user;
  }

  @decoratorGetSet(false)
  get pass() {
    return this._pass;
  }
}

const login = new Login(
  "test user",
  "test pass"
)

for (let key in login) {
  console.log("key", key);
  // console.log("value", login[key]);
}