function decoratorMetodo(target: unknown, propertyKey: string, descriptor: PropertyDescriptor) {
  console.log({ target, propertyKey, descriptor });
  console.log(descriptor.value);
  descriptor.value = (...args: unknown[]) => {
    return args.map(item => {
      return (<string>item).toUpperCase();
      // console.log(item);
    })
  };
}

class TratarMensagem {
  @decoratorMetodo
  dizerMensagem(...messages: string[]) {
    return messages;
  }
}

const instace = new TratarMensagem();
console.log(instace.dizerMensagem("Ola", "Seja bem-vindo", "Hcode"));