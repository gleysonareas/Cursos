import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, DoCheck, OnInit } from '@angular/core';

@Component({
  selector: 'v-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked {
  public title = 'v-14-app';
  public valor = 1;
  public destruir = true;

  //Primeiro estagio: construção
  constructor() {
  }

  //Segundo estagio: inicialização
  public ngOnInit(): void { }
  //Quarto estagio: Checkagem
  public ngDoCheck(): void {
    console.log('ngDoCheck');
  }
  //Quinto estagio: inicialização
  public ngAfterContentInit(): void {
    console.log('ngAfterContentInit');
  }
  //Sexto estagio: inicialização
  public ngAfterContentChecked(): void {
    console.log('ngAfterContentChecked');
  }
  //Setimo estagio: inicialização
  public ngAfterViewInit(): void {
    console.log('ngAfterViewInit');
  }
  //Oitavo estagio: inicialização
  public ngAfterViewChecked(): void {
    console.log('ngAfterViewChecked');
  }
  public Adicionar(): number {
    return this.valor++;
  }

  public destruirComponent(): void {
    this.destruir = false;
  }
}
