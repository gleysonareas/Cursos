import { Component, Input, OnChanges, OnDestroy } from '@angular/core';

@Component({
  selector: 'v-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnChanges, OnDestroy {

  @Input()
  public title = 'Bem vindo';

  //Terceiro estagio: detecção de mudanças
  public ngOnChanges(): void {
    alert(`Titulo alterado: ${this.title}`);
  }

  public ngOnDestroy(): void {
    console.log("Componente destruido");
  }
}
