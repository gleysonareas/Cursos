import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IUser } from '../../../shared/model/user.interface';
import { UserService } from "../../services/user.service";

@Component({
  selector: 'qb-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public imagePath: string = "../assets/buy-logo.jpg"
  public userAuthenticated: boolean;
  public user: IUser;
  public returnUrl: string = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: UserService
  ) {
    this.user = <IUser>{};
    this.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'];
  }

  ngOnInit(): void { }

  public enter(): void {
    this.userService.verifyUser(this.user).subscribe(
      data => {
      },
      err => {
      }
    );
    // if (this.user.email === 'gleysonareasdasilva@gmail.com' && this.user.password === 'Gyn.4539766') {
    //   sessionStorage.setItem('user-is-logged', '1');
    //   this.userAuthenticated = true;
    //   this.router.navigate([this.returnUrl]);
  }
}

