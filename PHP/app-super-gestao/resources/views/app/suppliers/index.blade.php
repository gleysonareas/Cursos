<h3>Fornecedores</h3>

<!-- Código comentado pela sintaxe do blade -->
{{-- Tudo que for escrito aqui não é interpretado pelo blade --}}

@php
    // Bloco if padrão do php
    // if (condition) {
    //     # code...
    // } elseif (condition) {
    //     # code...
    // } else {
    //     # code...
    // }
    // O Bloco if padrão executa se o retorno for true
    // if(isset($variavel)) {} Retorna true se a variavel estiver definida
    // if(empty($variavel)) {} Retorna true se a variavel estiver vazia
@endphp

{{-- {{ $suppliers }} --}}
{{-- @dd(suppliers) --}}

{{-- Bloco if no blade --}}
{{-- @if (count($suppliers) > 0 && count($suppliers) < 10)
    <h3>Existem alguns fornecedores cadastros</h3>
@elseif(count($suppliers) > 10)
    <h3>Existem varios fornecedores cadastros</h3>
@else
    <h3>Ainda não existem fornecedores cadastros</h3>
@endif --}}

{{-- Enquanto o unless executa se o retorno for false. O unless é um padrão de decisão do blade --}}

@isset($suppliers)
    {{-- @for ($i = 0; isset($suppliers[$i]); $i++) --}}
    {{-- @php $i = 0 @endphp --}}
    {{-- @while (isset($suppliers[$i])) --}}
    {{-- Iteração atual do loop:{{ $loop->iteration }} Para fins de didatica o objeto loop está disponivel apenas para laços foreach e forelse --}}
    Fornecedor: {{ $suppliers[$i]['nome'] }}
    <br>
    Status: {{ $suppliers[$i]['status'] }}
    <br>
    CNPJ: {{ $suppliers[$i]['cnpj'] ?? '' }} {{-- Sintaxe operador default --}}
    <hr>
    {{-- @if ($loop->first)
        Primeira interação do loop
    @endif
    @if ($loop->last)
        Ultima interação do loop
        Total de registros do array{{ $loop->count }}
    @endif --}}
    {{-- @php $i++ @endphp --}}
    {{-- @endwhile --}}
    {{-- @endfor --}}
    {{-- @if (!($suppliers[0]['status'] == 'S'))
        Fornecedor inativo
    @endif

    @unless ($suppliers[0]['status'] == 'S')
        Fornecedor inativo
    @endunless --}}
    {{-- @isset($suppliers[0]['cnpj'])
        CNPJ: {{ $suppliers[0]['cnpj'] }}
        @empty($suppliers[0]['cnpj'])
            - Vazio
        @endempty
    @endisset --}}

    {{-- Sintaxe switch case o valor deve ser passado nos parenteses do case --}}
    {{-- @switch()
        @case()
            @break
        @case()
            @break
        @default
    @endswitch --}}
@endisset

<br>
{{-- @for ($i = 0; $i < 10; $i++)
    {{ $i }} <br>
@endfor --}}

{{-- utilizando o @ antes dos caracteres de interpolação de dados dessa forma: @{{  }} estamos sinalizando
para o blade que não é para interpretar essa interpolação, ou seja os dados devem ser impressos como foram escritos no
html --}}
