<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Teve que ser feito da forma abaixo para evitar erro, porem o padrão correto seria 'PrincipalController@principal'
Route::get('/teste/{p1}/{p2}', [\App\Http\Controllers\TesteController::class, 'teste'])->name('site.teste');
Route::get('/', [\App\Http\Controllers\PrincipalController::class, 'principal'])->name('site.index');
Route::get('/about', [\App\Http\Controllers\AboutController::class, 'about'])->name('site.about');
Route::get('/contact', [\App\Http\Controllers\ContactController::class, 'contact'])->name('site.contact');
// Route::get('/login', [\App\Http\Controllers\LoginController::class, 'login'])->name('site.login');

//Dessa maneira podemos agrupar rotas em um determinado escopo de acesso
Route::prefix('/app')->group(function () {
  //   Route::get('/clients', [\App\Http\Controllers\ClientsController::class, 'clients'])->name('app.clients');
  Route::get('/suppliers', [\App\Http\Controllers\SuppliersController::class, 'suppliers'])->name('app.suppliers');
  //   Route::get('/products', [\App\Http\Controllers\ProductsController::class, 'products'])->name('app.products');//dessa maneira é possivel dar nomes as rotas no php
});

Route::fallback(function () {
  echo 'A rota acessada não existe ou você não possui acesso. <a href="' . route('site.index') . '">Clique aqui para voltar ao início</a>'; //Dessa maneira realizamos um redirect no controlador através de fallback
});


//Dessa maneira realizamos a passagem de parametro na rota, adcionando interrogação na frente do parametro o mesmo se torna opcional, sempre realizando essa atribuição da direita para esquerda
// Route::get(
//   '/contact/{nome?}/{categoria_id}',
//   function (
//     string $nome = 'Desconhecido',
//     int $categoria_id = 1
//   ) {
//     echo "Estamos aqui: $nome - $categoria_id";
//   }
// )
//   ->where('categoria_id', '[0-9]+')
//   ->where('nome', '[A-Za-z]+');
// logo após o metodo da rota podemos realizar a chamada do metodo where para aplicar validação nos parametros que estão sendo passados na rota.
