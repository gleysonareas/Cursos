<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SuppliersController extends Controller
{
    public function index()
    {
        // $suppliers = ['Fornecedor 1'];
        $suppliers = [
            0 => [
                'nome' => 'Fornecedor 1',
                'status' => 'N',
                'cnpj',
            ]
        ];

        // Podemos utilizar ternarios para fins de validação no php tbm e inclusive incluir uma validação junto da outra mesmo não sendo recomendado
        // condição ? se verdade : se falso;
        // condição ? se verdade : (condição ? se verdade : se falso);

        return view('app.suppliers.index', compact('suppliers'));
    }
}
