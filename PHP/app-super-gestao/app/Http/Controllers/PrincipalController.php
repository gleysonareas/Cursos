<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PrincipalController extends Controller
{
    public function principal()
    {
        // return 'Olá mundo!';
        return view('site.principal');
    }
}
