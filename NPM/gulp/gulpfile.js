const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const autoprefixer = require("gulp-autoprefixer");
const browserSync = require("browser-sync").create();
const concat = require("gulp-concat");

function compileSass() {
  return gulp
    .src("css/scss/**/*.scss")
    .pipe(
      sass({
        outputStyle: "compressed",
      })
    )
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(gulp.dest("css/"))
    .pipe(browserSync.stream());
}

gulp.task("sass", compileSass);

function gulpJs() {
  return gulp.src("js/**/*.js").pipe(concat("main.js")).pipe(gulp.dest("js/"));
}

gulp.task("mainJs", gulpJs);

function browser() {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
}

gulp.task("browser-sync", browser);

function watch() {
  gulp.watch("css/scss/*.scss", compileSass);
  gulp.watch("js/main/*.js", gulpJs);
  gulp.watch(["*.html"]).on("change", browserSync.reload);
}

gulp.task("watch", watch);

gulp.task("default", gulp.parallel("watch", "browser-sync", "sass", "mainJs"));
